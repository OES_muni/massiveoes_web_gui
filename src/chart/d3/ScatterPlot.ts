import MassiveD3 from './main';
import DataSet from './DataSet';
import * as d3 from 'd3';
import Point from './Point';
import {ScaleLinear} from 'd3-scale';
import {lasso} from 'd3-lasso';
import MassiveD3Options from './Options/MassiveD3Options';

export default class ScatterPlot extends MassiveD3 {

    private lassoTool: any;

    private pointsSelected: (points: Point[]) => {};

    private currentSelectedPoints: Point[] = [];

    private xScale: ScaleLinear<number, number>;
    private yScale: ScaleLinear<number, number>;

    constructor(svg: SVGElement, options: MassiveD3Options) {
        super(svg, options);
        // lasso plugin rely on global d3 variable, remove after lasso plugin update
        window['d3'] = d3;
    }

    private enableLasso() {
        this.lassoTool = lasso()
            .closePathDistance(75)
            .on('start', () => this.lasso_start())
            .on('draw', () => this.lasso_draw())
            .on('end', () => this.lasso_end())
            .targetArea(
                this.svgFrame.append('rect')
                    .attr('id', 'scatter-lasso-area')
                    .attr('width', this.options.FrameWidth)
                    .attr('height', this.options.FrameHeight)
                    .style('opacity', 0)
            );
    }

    private lasso_start(): void {
        this.lassoTool.items().style('fill', null);
    }

    private lasso_draw(): void {
        this.lassoTool.possibleItems().style('fill', (d: any) => d.color);
    }

    private lasso_end(): void {
        this.lassoTool.items()
            .style('fill', (d: any) => d.color);

        this.lassoTool.selectedItems().attr('r', 7);
        this.lassoTool.notSelectedItems().attr('r', 2.5);

        this.currentSelectedPoints = this.lassoTool.selectedItems()['_groups'][0].map((d: any) => d['__data__']);
        this.pointsSelected(this.currentSelectedPoints);
    }

    protected enablePlugins(xScale: ScaleLinear<number, number>, yScale: ScaleLinear<number, number>): void {
        this.enableLasso();
    }

    protected enableDotClick() {
        d3.selectAll('.dot').on('click', (p: Point, i: number) => {
            let dot = d3.select('#dot_' + i);
            let arrayIndex = this.currentSelectedPoints.indexOf(p);
            if (arrayIndex > 0) {
                this.currentSelectedPoints.splice(arrayIndex, 1);
                dot.attr('r', 2.5);
            } else {
                this.currentSelectedPoints.push(p);
                dot.attr('r', 7);
            }
            this.pointsSelected(this.currentSelectedPoints);
        });
    }

    public draw(dataSets: DataSet[]) {
        this.dataSets = dataSets;
        let points = (this.options.scaleByFirstDataSet)
            ? this.dataSets[0].data
            : [].concat.apply([], this.dataSets.filter((d: DataSet) => d.visible).map((d: DataSet) => d.data));

        let xScale = this.xScale = d3.scaleLinear()
        // extend domain, to move points from corners
            .domain(this.extendDomain(d3.extent(points, (p: Point) => p.x)))
            .range([0, this.options.FrameWidth]);
        let yScale = this.yScale = d3.scaleLog()
            .base(10)
            .domain(d3.extent(points, (p: Point) => p.y))
            .range([this.options.FrameHeight, 0]);

        this.drawAxises(xScale, yScale);
        this.enablePlugins(xScale, yScale);
        this.drawData(dataSets, xScale, yScale);
    }

    public drawRegressionLine(data: {x: number[], y: number[]}) {
        this.removeRegressionLine();
        if (data.x.length < 2) {
            return;
        }

        this.svgFrame.append<SVGLineElement>('line')
            .attr('id', 'scatter-regression-line')
            .attr('x1', this.xScale(data.x[0]))
            .attr('y1', this.yScale(data.y[0]))
            .attr('x2', this.xScale(data.x[data.x.length - 1]))
            .attr('y2', this.yScale(data.y[data.y.length - 1]))
            .attr('stroke-width', 2)
            .attr('stroke', 'black');
    }

    public removeRegressionLine() {
        d3.select('#scatter-regression-line').remove();
    }

    protected extendDomain(domain: [number, number], c = 0.02) {
        c = Math.abs(domain[0] - domain[1]) * c;
        domain[0] = domain[0] - c;
        domain[1] = domain[1] + c;
        return domain;
    }

    protected drawData(dataSets: DataSet[], xScale: ScaleLinear<number, number>, yScale: ScaleLinear<number, number>): void {
        let container = this.drawDataLinesContainer();

        let i = 0;
        for (let dataSet of dataSets.filter((d: DataSet) => d.visible)) {
            for (let point of dataSet.data) {
                (<any>point).color = dataSet.color;
                container.append<SVGCircleElement>('circle')
                    .datum(point)
                    .attr('id', () => 'dot_' + i) // added
                    .attr('class', 'dot')
                    .attr('r', 2.5)
                    .attr('cx', (p: Point) => xScale(p.x))
                    .attr('cy', (p: Point) => yScale(p.y))
                    .style('fill', () => dataSet.color);
                i++;
            }
        }
        this.lassoTool.items(d3.selectAll('.dot'));
        this.enableDotClick();
        container.call(this.lassoTool);
    }
}