export default class MassiveD3OptionsMargin {
    public top: number = 0;
    public right: number = 0;
    public bottom: number = 0;
    public left: number = 0;

    get y(): number {
        return this.top + this.bottom;
    }

    get x(): number {
        return this.left + this.right;
    }

    public constructor(margin: number[] = null) {
        if (margin !== null) {
            switch (margin.length) {
                case 1:
                    this.top = margin[0];
                    this.right = margin[0];
                    this.bottom = margin[0];
                    this.left = margin[0];
                    break;
                case 2:
                    this.top = this.bottom = margin[0];
                    this.right = this.left = margin[1];
                    break;
                case 3:
                    this.top = margin[0];
                    this.right = margin[1];
                    this.bottom = margin[2];
                    break;
                case 4:
                    this.top = margin[0];
                    this.right = margin[1];
                    this.bottom = margin[2];
                    this.left = margin[3];
                    break;
            }
        }
    }
}