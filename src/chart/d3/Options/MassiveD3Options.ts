import MassiveD3OptionsMargin from './MassiveD3OptionsMargin';

export default class MassiveD3Options {
    public margin: MassiveD3OptionsMargin = new MassiveD3OptionsMargin([20, 50]);
    public width: number;
    public height: number;
    public scaleByFirstDataSet: boolean = true;

    get FrameWidth(): number {
        return this.width - this.margin.x;
    }

    get FrameHeight(): number {
        return this.height - this.margin.y;
    }

    public static fromPrimitive(primitive): MassiveD3Options {
        let o = new MassiveD3Options();
        o.margin = new MassiveD3OptionsMargin(primitive['margin']);
        o.width = primitive['width'];
        o.height = primitive['height'];
        o.scaleByFirstDataSet = primitive['scaleByFirstDataSet'];
        return o;
    }
}