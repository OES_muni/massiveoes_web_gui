import DataSet from './DataSet';
import * as d3 from 'd3';
import Point from './Point';
import {ScaleLinear} from 'd3-scale';
import LineChart from './LineChart';
import MassiveD3Options from './Options/MassiveD3Options';

export default class LinearizationChart extends LineChart {

    private linearizationLines = [];

    public newLinearizationPoint = (measurement: number, simulation: number) => {
    }

    public constructor(svg: SVGElement, options: MassiveD3Options) {
        super(svg, options);
        this.enableKeys();
    }

    private enableKeys(): void {
        d3.select('body').on('keydown', () => {
            switch (d3.event.keyCode) {
                /*Enter*/
                case 13:
                    this.newLinearizationPoint(this.linearizationLines[0].x, this.linearizationLines[1].x);
                    break;
                /*ESC*/
                case 27:
                    break;
                default:
                    return;
            }
            this.clearLinearizationLines();
        });
    }

    private clearLinearizationLines(): void {
        this.linearizationLines.map((e: LinearizationLineData) => {
            e.svgLine.remove();
            this.linearizationLines = [];
        });
    }

    private mouseDownCallback(container, xScale: ScaleLinear<number, number>, yScale: ScaleLinear<number, number>): void {
        let x = xScale.invert(d3.mouse(<any>container.node())[0]);
        if (this.dataSets.length === this.linearizationLines.length) {
            this.clearLinearizationLines();
        }
        let line = container.append('line')
            .attr('class', 'line')
            .attr('x1', xScale(x))
            .attr('x2', xScale(x))
            .attr('y1', yScale(yScale.domain()[0]))
            .attr('y2', yScale(yScale.domain()[1]))
            .attr('style', 'stroke:' + this.dataSets[this.linearizationLines.length].color + ';stroke-width:' + (1 / this.currentZoomKFactor).toString());
        this.linearizationLines.push(new LinearizationLineData(line, x, this.linearizationLines.length));
    }

    protected drawData(dataSets: DataSet[], xScale: ScaleLinear<number, number>, yScale: ScaleLinear<number, number>): void {
        let container = this.drawDataLinesContainer();

        let overlay = this.svgFrame.append('rect')
            .attr('class', 'overlay')
            .attr('width', this.options.FrameWidth)
            .attr('height', this.options.FrameHeight)
            .attr('style', 'fill:transparent;')
            .on('mousedown', () => {
                if (d3.event.ctrlKey) {
                    this.mouseDownCallback(container, xScale, yScale);
                    d3.event.stopPropagation();
                }
            });

        for (let dataSet of dataSets.filter((d: DataSet) => d.visible)) {
            container.append<SVGPathElement>('path')
                .datum(dataSet.data)
                .attr('class', 'line')
                .attr('fill', 'none')
                .attr('stroke', dataSet.color)
                .attr('stroke-width', 1)
                .attr('d', d3.line<Point>()
                    .x((d: Point) => xScale(d.x))
                    .y((d: Point) => yScale(d.y))
                );
        }
    }
}

class LinearizationLineData {
    constructor(public svgLine: any, public x: number, dataSetIndex: number) {
    }
}