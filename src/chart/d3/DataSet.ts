import Point from './Point';

export default class DataSet {
    public name: string = 'black set';
    public data: Point[] = [];
    public color: string = 'black';
    public visible: boolean = true;
}