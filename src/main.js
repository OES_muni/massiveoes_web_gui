import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'
import VueSocketio from 'vue-socket.io'

import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

Vue.use(VueMaterial)
Vue.use(VueSocketio, 'ws://localhost:8888', store)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
