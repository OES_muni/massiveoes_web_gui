const parameterMap = {
  'wav_start': {
    name: 'Starting wavelength',
    unit: 'nm'
  },
  'wav_step': {
    name: 'Wavelength step',
    unit: 'nm'
  },
  'wav_2nd': {
    name: 'Quadratic correction',
    unit: ''
  },
  'slitf_gauss': {
    name: 'Gaussian HWHM of the slit function',
    unit: ''
  },
  'slitf_lorentz': {
    name: 'Lorentzian HWHM of the slit function',
    unit: ''
  },
  'baseline': {
    name: 'Baseline',
    unit: ''
  },
  'baseline_slope': {
    name: 'Baseline slope',
    unit: ''
  },
  '$_intensity': {
    name: '$ Intensity',
    unit: ''
  },
  '$_Tvib': {
    name: '$ vibrational temperature',
    unit: 'K'
  },
  '$_Trot': {
    name: '$ rotational temperature',
    unit: 'K'
  }
}

export function parameterLabel (parameterName) {
  return findParameter(parameterName).name
}

export function parameterUnit (parameterName) {
  return findParameter(parameterName).unit
}

function findParameter (parameterName) {
  if (parameterMap.hasOwnProperty(parameterName)) {
    return parameterMap[parameterName]
  }
  let simulationParameterNameParts = parameterName.split('_')
  if (parameterMap.hasOwnProperty('$_' + simulationParameterNameParts[1])) {
    let p = JSON.parse(JSON.stringify(parameterMap['$_' + simulationParameterNameParts[1]]))
    p.name = p.name.replace('$', simulationParameterNameParts[0])
    return p
  }
}
