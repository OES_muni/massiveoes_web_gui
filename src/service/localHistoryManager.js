export function getHistory (namespace) {
  if (namespace != null) {
    let data = JSON.parse(localStorage.getItem(namespace))
    if (data !== null) {
      return data
    }
  }
  return {
    root: '/',
    items: []
  }
}

export function getRoot (namespace) {
  return getHistory(namespace).root
}

export function setRoot (namespace, value) {
  let history = getHistory(namespace)
  history.root = value
  localStorage.setItem(namespace, JSON.stringify(history))
}

export function addItemToHistory (namespace, value) {
  let history = getHistory(namespace)
  history.items = history.items.filter(item => item !== value)
  history.items.unshift(value)
  localStorage.setItem(namespace, JSON.stringify(history))
}
