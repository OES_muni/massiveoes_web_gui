const state = {
  workspace: {
    active_measurement_set: NaN,
    main_plot_data: {
      measurement_data: {x: [], y: []},
      simulation_data: {x: [], y: []},
      residual_data: {x: [], y: []}
    },
    measurement_set_list: [],
    parameters: [],
    expanded_parameters: [],
    simulations: [],
    fit_parameters: {
      max_iterations: NaN,
      fit_methods_list: [],
      active_fit_method: null
    }
  }
}

const getters = {
  measurementSets: (state) => state.workspace.measurement_set_list,
  activeMeasurementSet: (state) => state.workspace.active_measurement_set,
  mainChartData: (state) => state.workspace.main_plot_data,
  mainFormRowData: (state) => state.workspace.parameters,
  expandedMainFormRows: (state) => state.workspace.expanded_parameters,
  fitParameters: (state) => state.workspace.fit_parameters,
  hasSimulation: (state) => state.workspace.simulations.length > 0,
  simulations: (state) => state.workspace.simulations
}

const actions = {
  setActiveMeasurementSet: ({ commit }) => commit('SET_ACTIVE_MEASUREMENT_SET'),
  expandFormRow: ({ commit }) => commit('EXPAND_FORM_ROW'),
  expandAllFormRows: ({ commit }) => commit('EXPAND_ALL_FORM_ROWS'),
  shrinkAllFormRows: ({ commit }) => commit('SHRINK_ALL_FORM_ROWS'),
  shrinkFormRow: ({ commit }) => commit('SHRINK_FORM_ROW')
}

const mutations = {
  SOCKET_WORKSPACE__UPDATE: (state, data) => {
    state.workspace = Object.assign(state.workspace, data)
  },
  SET_ACTIVE_MEASUREMENT_SET: (state, set) => {
    state.workspace.active_measurement_set = set
  },
  EXPAND_FORM_ROW: (state, rowData) => {
    rowData = rowData['parameter_name']
    let index = state.workspace.expanded_parameters.indexOf(rowData)
    if (index > -1) {
      state.workspace.expanded_parameters[index] = rowData
    } else {
      state.workspace.expanded_parameters.push(rowData)
    }
  },
  EXPAND_ALL_FORM_ROWS: (state) => {
    state.workspace.expanded_parameters = []
    state.workspace.parameters.forEach(e => {
      state.workspace.expanded_parameters.push(e['parameter_name'])
    })
  },
  SHRINK_ALL_FORM_ROWS: (state) => {
    state.workspace.expanded_parameters = []
  },
  SHRINK_FORM_ROW: (state, rowData) => {
    state.workspace.expanded_parameters.splice(state.workspace.expanded_parameters.indexOf(rowData['parameter_name']), 1)
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
