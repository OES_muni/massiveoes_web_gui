import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import workspace from './modules/workspace'
import linearization from './modules/linearization'
import bpInspector from './modules/bpInspector'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    app,
    workspace,
    linearization,
    bpInspector
  }
})
