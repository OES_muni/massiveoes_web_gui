import Point from '../chart/d3/Point'

export const DATA_SET_TYPE = {
  MEASUREMENT: {
    label: 'Measured values',
    color: '#4CAF50'
  },
  SIMULATION: {
    label: 'Simulation',
    color: '#3F51B5'
  },
  RESIDUAL: {
    label: 'Residuals',
    color: '#607D8B'
  }
}

export const createChartDataSet = function (dataSetType, data) {
  let dataSet = dataSetType
  let y = data.y
  dataSet['data'] = data.x.map((val, index) => new Point(val, y[index]))
  dataSet['visible'] = true
  return dataSet
}
