import Vue from 'vue'
import Router from 'vue-router'
import Workspace from '@/components/workspace/Workspace'
import Linearization from '@/components/Linearization'
import BPInspector from '@/components/bp-inspector/BPInspector'
import FileViewer from '@/components/file-viewer/FileViewer'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'FileViewer',
      component: FileViewer
    },
    {
      path: '/workspace',
      name: 'Workspace',
      component: Workspace
    },
    {
      path: '/linearize',
      name: 'Linearization tool',
      component: Linearization
    },
    {
      path: '/boltzmann',
      name: 'Boltzmann plot tool',
      component: BPInspector
    }
  ]
})
