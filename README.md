# massiveOES Web GUI

> Web GUI for masiveOES emission spectra analysis software

---

##### This GUI is part of [massiveOES](https://bitbucket.org/OES_muni/massiveoes), and needs running massiveOES API. 


## Build Setup
>For following steps you need Node.js with npm installed on your system. 
``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```